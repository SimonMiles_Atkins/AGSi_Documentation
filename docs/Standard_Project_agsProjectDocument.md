


# AGSi Standard / Project

## agsProjectDocument

### Object description

Metadata for the individual documents or references contained within a document set.

The parent object of [agsProjectDocument](./Standard_Project_agsProjectDocument.md) is [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agsProjectDocument](./Standard_Project_agsProjectDocument.md) has the following attributes:


- [title](#title)
- [description](#description)
- [author](#author)
- [client](#client)
- [originalReference](#originalreference)
- [systemReference](#systemreference)
- [revision](#revision)
- [date](#date)
- [status](#status)
- [statusCode](#statuscode)
- [documentURI](#documenturi)
- [documentSystemURI](#documentsystemuri)
- [remarks](#remarks)


### Attributes

#### title
Original title on the document  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Factual ground investigation report, Gotham City Metro Purple Line, C999 Package A, Volume 1 of 3``

#### description
Further description if required. Typically what the document is commonly known as, given that the formal title may be verbose. Alternatively use for name/title given in project documentation system if this differs from original title.  
*Type:* string  
*Example:* ``Package A factual report, Volume 1``

#### author
The original author of the document  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Boring Drilling Ltd``

#### client
The original commissioning client for the document  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

#### originalReference
Original reference shown on the document, if different from reference used by project document system (see systemReference).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``12345/GI/2``

#### systemReference
Document reference used in the project document management system. May differ from original reference shown on document.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``C999-BDL-AX-XX-RP-WG-0002``

#### revision
Revision reference (typically in accordance with ISO19650 or BS1192)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P2``

#### date
Date of the document (current revision)  
*Type:* string (date)  
*Condition:* Recommended  
*Example:* ``2018-09-06``

#### status
Status as indicated on or within the document, typically following recommendations of BS8574 if applicable.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Final``

#### statusCode
Status code, typically in accordance with ISO19650, or BS1192 suitability code  
*Type:* string  
*Condition:* Recommended  
*Example:* ``S2``

#### documentURI
URI (link address) for the document. This will be a relative link if document included with the AGSi data set. For a public domain published reference, the link should be provided here.  
*Type:* string (uri)  
*Condition:* Recommended  
*Example:* ``docs/GI/C999-BDL-AX-XX-RP-WG-0002.pdf``

#### documentSystemURI
URI (link address) for the location of the document within the project document management system. To be used if documentURI attribute used for relative link.  
*Type:* string (uri)  
*Condition:* Recommended  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-BDL-AX-XX-RP-WG-0002``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``

