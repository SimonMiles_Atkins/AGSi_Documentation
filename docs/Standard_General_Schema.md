# AGSi Standard / General

## Schema

### Schema requirements

AGSi data shall only be valid if it is arranged in accordance with the schema defined in this standard.

A general high level description of the schema is given below. Full details of schema are given in the following sections:

* [Root](./Standard_Root_Intro.md)
* [Project](./Standard_Project_Intro.md)
* [Model](./Standard_Model_Intro.md)
* [Geometry](./Standard_Geometry_Intro.md)
* [Data](./Standard_Data_Intro.md)

### Description of schema

The AGSi schema is based on an object model, with **objects** generally representing physical or virtual entities.

Objects have a number of **attributes** (alternatively: properties) that describe the object in more detail.

The proposed schema is hierarchical, i.e. it includes objects embedded (nested) within other objects.

In other cases, relationships between objects are defined by cross-reference links, i.e. links formed via object attributes.

The data that makes up each AGSi file is contained within a single
[root object](./Standard_Root_Intro.md).
Within this [root object](./Standard_Root_Intro.md)
are found two objects that contain general metadata:
[schema](./Standard_Root_schema.md) and
[file](./Standard_Root_file.md).
The remaining objects within the root object are organised into the following **groups** according to function:

* [Project](./Standard_Project_Intro.md)
* [Model](./Standard_Model_Intro.md)
* [Geometry](./Standard_Geometry_Intro.md)
* [Data](./Standard_Data_Intro.md)

This is illustrated in the following diagram.

!!! Note
    This diagram is a UML class diagram, which is the format adopted for schema digrams in this documentation. Further information, including how to interpret such diagrams, is provided in the
    [Guidance](./Guidance_General_UML.md).

![Schema overall summary UML diagram](./images/overall_summary_UML.svg)

Each group is effectively a standalone subschema within the overall schema.
Links between groups are made by reference as required.

A diagram showing all of the objects in the AGSi schema is given.

![Schema all objects summary UML diagram](./images/ALL_summary_UML_simple.svg)

The [Model](./Standard_Model_Intro.md) group may be considered to be the core part of the schema as this is used to assemble models.

The [Data](./Standard_Data_Intro.md) and
[Geometry](./Standard_Geometry_Intro.md) groups provide resources, i.e. the detailed properties/parameters and geometry respectively, that the core group objects can reference. The
[Project](./Standard_Project_Intro.md) group
provides general metadata, some of which is core data but some is resources. This concept is illustrated below.

![general](./images/general_core_resource.svg)
