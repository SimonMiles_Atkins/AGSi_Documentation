# AGSi Standard / Model

## Model rules and conventions

This section details rules and conventions applicable to the Model group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.

### Models

There may be more than one model defined in an AGSi file/data set.

For further information on what is considered to be a single model in an AGSi context,
please refer to [Guidance on usage, Model - Overview](./Guidance_Model_General.md).

If there is only one model in a file/data set, then the
[agsModel](./Standard_Model_agsiModel.md)
object must be included within an array given that the parent attribute data type is array, in accordance with
[General rules and conventions](./Standard_General_Rules.md#arrays).
Replacing the array with a single object is not permitted.

### Model coordinate system

The coordinate system for each model should be defined using the
[*coordSystemID*](./Standard_Model_agsiModel.md#coordsystemid) attribute of the
[agsiModel](./Standard_Model_agsiModel.md) object, referencing the
[*systemID*](./Standard_Project_agsProjectCoordinateSystem.md#systemID) identifier for the relevant
[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md#systemID) object.

If the coordinate system(s) are not defined using the above method, then this information shall be conveyed in supporting documentation.

Further information on coordinate systems is provided in
[Project rules and conventions](./Standard_Project_Rules.md#coordinate-systems)


### Model elements

Model elements ([agsiModelElement](./Standard_Model_agsiModelElement.md) objects)
comprising volumetric units that cross or overlap are permitted in AGSi. Whether such a model is correct or appropriate is for the user to determine.

Each model element ([agsiModelElement](./Standard_Model_agsiModelElement.md)) shall have geometry assigned to it via the
[*geometryID*](./Standard_Model_agsiModelElement.md#geometryid) attribute of the
[agsiModelElement](./Standard_Model_agsiModelElement.md) object, referencing the
*geometryID* identifier for the relevant object in the Geometry group.  It is recommended that the target Geometry group object class is identified using the
[*geometryObject*](./Standard_Model_agsiModelElement.md#geometryobject) attribute of
[agsiModelElement](./Standard_Model_agsiModelElement.md) as some software/applications may require this.

### Model element limiting area

The [*areaLimitGeometryID*](./Standard_Model_agsiModelElement.md#arealimitgeometryid
) attribute of [agsiModelElement](./Standard_Model_agsiModelElement.md)
allows a limiting plan area for the element to be defined by reference to a closed polygon object. The polygon acts as a vertical 'cookie cutter' clipping the element boundary, i.e. the model element is the part contained within the polygon only.

This feature should be used with caution as it may not be supported by all software/applications.
Project requirements for use of this attribute should be confirmed in the
[specification](./Standard_General_Specification.md).

### Model boundary

An [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object defines the model boundary, i.e. the maximum extent of the model. Any model elements or parts of model elements lying outside the boundary are deemed to be not part of the model.

Only model boundaries with vertical sides are supported by this method.

Only one boundary object per [agsiModel](./Standard_Model_agsiModel.md) is permitted.

The plan (XY) extent of the model should be defined by ONE of the following methods:

- limiting co-ordinates (minX, maxX, minY and maxY), or
- closed polygon (referenced geometry object)

The top and bottom (base) of the model should be defined by ONE of the following methods:

- elevation (Z value) of a plane, or
- surface (referenced geometry object)

Definition of the top surface of the model, which is typically the ground surface (terrain) may not be required in some cases. This may be software/application dependent.

Use of this [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
object is optional if the model elements are already curtailed at the model boundary.

Some aspects of this feature may not be supported by all software/applications.
Project requirements for use of model boundaries, including whether a top boundary is required, should be described in the
[specification](./Standard_General_Specification.md).

### Subsets

Each model may optionally be divided up into model subsets.
These subsets are defined using [agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects.

For further information on how subsets can be used,
please refer to [Guidance on usage, Model - Overview](./Guidance_Model_General.md).

Each model element can belong to no more than one subset. The subset each element belongs to, if any, is defined using the
[*subsetID*](./Standard_Model_agsiModelElement.md#subsetid)
attribute of
[agsiModelElement](./Standard_Model_agsiModelElement.md)
which references the
[*subsetID*](./Standard_Model_agsiModelSubset.md#subsetid)
identifier of the relevant
[agsiModelSubset](./Standard_Model_agsiModelSubset.md) object.

Subsets may not be supported by all software/applications.
Project requirements for use of subsets should be described in the
[specification](./Standard_General_Specification.md).
