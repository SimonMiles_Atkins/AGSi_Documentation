
# AGSi Standard / Data

## Data rules and conventions

This section details rules and conventions applicable to the Data group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.

### Properties and parameters

For the purposes of the schema, a distinction between properties and
parameters has been made as follows:

#### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.

#### Parameter

An interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

### Incorporation of factual data in AGSi

AGSi should not incorporate the full factual data used to derive the model.

Factual data should be provided in AGS (factual) format file(s) which should be identified as documents using [agsProjectDocument](./Standard_Project_agsProjectDocument.md) objects. Reference to these documents can be made from several places in the schema, including the [*documentSetID*](./Standard_Data_agsiData.md#documentsetid) attribute
of [agsiData](./Standard_Data_agsiData.md).

AGSi may include summaries of the factual data using the [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object.

### Codes

Use of the [AGSi list of standard codes](./Codes_Codelist.md) is recommended. If this is the case, use of [agsProjectCode](./Standard_Project_agsProjectCode.md) objects to define the codes is optional, provided that use of the standard list is confirmed by providing a link to it in the [*codeDictionary*](./Standard_Data_agsiData.md#documentsetid) attribute
of [agsiData](./Standard_Data_agsiData.md)

If the [AGSi list of standard codes](./Codes_Codelist.md)
is used but additional non-standard codes are required for items not covered, then the additional codes shall be defined using [agsProjectCode](./Standard_Project_agsProjectCode.md).

If an alternative code list is used, then the codes shall be defined using [agsProjectCode](./Standard_Project_agsProjectCode.md) objects.

Project requirements for codes should be described in the
[specification](./Standard_General_Specification.md).

### Use of (data) case

Use of the *caseID* attribute of
[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) is optional.
Either a code or text may be input. If a code is used, this should be defined using the [agsiDataCase](./Standard_Data_agsiDataCase.md) object.

Examples of how *caseID* may be used are given in the
[Guidance (Data-General)](./Guidance_Data_General.md#use-of-case).

Project requirements for use of case should be described in the
[specification](./Standard_General_Specification.md).
