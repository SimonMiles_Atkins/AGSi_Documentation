


# AGSi Standard / Project

## agsProjectCode

### Object description

Project specific codes used by some parts of the schema. Typically used for project specific ABBR codes inherited from AGS factual data. Inclusion of standard AGS ABBR codes used is optional (unless required by specification). Not to be used for property and parameter codes in the Data group (use [agsiDataCode](./Standard_Data_agsiDataCode.md) object instead).  

The parent object of [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) is [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCode.md) has the following attributes:


- [code](#code)
- [description](#description)
- [remarks](#remarks)


### Attributes

#### code
Project specific code used in this file/data set. All codes in a set should be unique.   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``MG``

#### description
Short description of what the code represents.  
*Type:* string  
*Condition:* Required  
*Example:* ``Made Ground``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``

