


# AGSi Standard / Geometry

## agsiGeometryAreaFromLines

### Object description

An [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) object defines an element as the area between top and/or bottom lines. This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for full details of how the area should be interpreted.

The parent object of [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md)
- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)

[agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [topGeometryID](#topgeometryid)
- [bottomGeometryID](#bottomgeometryid)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by Model and Geometry objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``SectionAAGCC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Section AA, Gotham City Clay``

#### topGeometryID
Identifies the geometry for top line, linking to [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for further details.  
*Type:* string (reference to geometryID for an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if bottomGeometryID not used  
*Example:* ``SectionAAGCCTop``

#### bottomGeometryID
Identifies the geometry for bottom line, normally linking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object. Definition of both top and bottom surfaces is strongly recommended to minimise the risk of error. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for further details.  
*Type:* string (reference to geometryID for an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Condition:* Required if topGeometryID not used  
*Example:* ``SectionAAGCCBase``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

