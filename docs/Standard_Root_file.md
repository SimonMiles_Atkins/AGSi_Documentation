


# AGSi Standard / Root

## file

### Object description

Meta data for this file (or data set, if not transferred as a file as such).  The file/data set should be treated as a document in accordance with standards established for the project. The attributes provided align with good practice BIM, in accordance with ISO19650. It is recommended that, where possible, this object is output at the top of the file, after the schema object, for human readability.

The parent object of file is [root](./Standard_Root_Intro.md)

file has the following attributes:


- [title](#title)
- [projectTitle](#projecttitle)
- [description](#description)
- [producedBy](#producedby)
- [fileUUID](#fileuuid)
- [fileURI](#fileuri)
- [reference](#reference)
- [revision](#revision)
- [date](#date)
- [status](#status)
- [statusCode](#statuscode)
- [systemURI](#systemuri)
- [madeBy](#madeby)
- [checkedBy](#checkedby)
- [approvedBy](#approvedby)
- [remarks](#remarks)


### Attributes

#### title
Title of the file/data set (as used for document management system)  
*Type:* string  
*Condition:* Required  
*Example:* ``Stage 3 Sitewide Ground models``

#### projectTitle
Name of project (as used for document management system)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line, C999 Geotechnical Package X``

#### description
Additional description of the file/data set, if required.  
*Type:* string  
*Example:* ``Geological model and geotechnical design models produced for Stage 4 design``

#### producedBy
Organisation that produced this file/data set  
*Type:* string  
*Condition:* Required  
*Example:* ``ABC Consultants``

#### fileUUID
Universal unique identifier (UUID) for the file or data set  
*Type:* string  
*Example:* ``98e17952-c99d-4d87-8f01-8ba75d29b6ad``

#### fileURI
URI for the location of this file/data set within the project document system  
*Type:* string (uri)  
*Condition:* Recommended  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-ABC-AX-XX-M3-CG-1234``

#### reference
Document reference (typically in accordance with ISO19650, BS1192 or project standards)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``C999-ABC-AX-XX-M3-CG-1234``

#### revision
Revision reference (typically in accordance with ISO19650 or BS1192 or project standards)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P1``

#### date
Date of production of this data set/file  
*Type:* string (date)  
*Condition:* Recommended  
*Example:* ``2018-10-05``

#### status
Status, typically following recommendations of BS8574.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Final``

#### statusCode
Status code in accordance with ISO19650 (or BS1192 suitability code)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``S2``

#### systemURI
URI (link address) for the location of this file/data set within the project document management system.  
*Type:* string (uri)  
*Condition:* Recommended  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-BDL-AX-XX-RP-WG-0002``

#### madeBy
Person(s) identified as originator  
*Type:* string  
*Example:* ``A Green``

#### checkedBy
Person(s) identified as checker  
*Type:* string  
*Example:* ``P Brown``

#### approvedBy
Person(s) identified as approver  
*Type:* string  
*Example:* ``T Black``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

