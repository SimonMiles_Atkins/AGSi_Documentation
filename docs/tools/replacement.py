# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 18:14:03 2019
Modified and turned into sub 23/11/20

@author: neil.chadwick
"""

def replaceObjectLink(originaltxt,replacementlist):
    
    # This sub is a replacement tool specifically geares towards creating links
    # based on simple text. Written to creat links for objects by replacing, e.g 
    # agsiData with the text for the full link
    # Target is individual words, e.g. for target aaa, then aaa will be replaced
    # by bbaaabb will reamin untouched. Also, if target is precedded by " or # 
    # it will remain untouched. I think there may be other exceptions to 
    # (the regex is the key to this)
    # See other sub if you just want a 'simple' replacement (but use that 
    # one with extreme care!)
    # Input = csv file as a list of: 'target string','replacement string' 
    
    import re

       
    #Read files to be processed
    replacetxt = ''
    replacetextlines = []
    
     
    # old - for full file
    #Do replacements, loop through each word in list of replacements
    #Note that we need to pad word with a space becuase need to capture # before
    # (not elegent, but seems to work)
    # May be problem - won't reconginse [ etc!]
    #for replaceword in replacementlist:
    #    replaceword[0] = ' ' + replaceword[0]
    #    replaceword[1] = ' ' + replaceword[1]
    #    regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
    #    regexreplace =  replaceword[1]  
    #    replacetext = re.sub(regexfind, regexreplace, replacetext)   
    
    # Try spltting into ine by line so we can intercept title lines
    
    for replacetextline in originaltxt.split('\n'):
       
        #Do replacements, loop through each word in list of replacements
        # Check line does not start with # - if it does, ignore
            
        if replacetextline== None:
            replacetextlines = replacetextlines.append('\n') 
        elif replacetextline.startswith('#'):
            replacetextlines.append(replacetextline) 
        else:
            for replaceword in replacementlist:
                regexfind = r'(?<!\[|\#|\"|/|\(){}\b'.format(replaceword[0])
                regexreplace =  replaceword[1]  
                replacetextline = re.sub(regexfind, regexreplace, replacetextline)     
            replacetextlines.append(replacetextline)      
        
    #return new text
    replacetxt = replacetxt + '\n' + '\n'.join(replacetextlines)
    return replacetxt


### This is the simple version - strang text strng repalcement, not iff no buts
### Use with caution!

def replaceSimple(originaltxt,replacementlist):
    
    #  Simple version - simply text strings
    
      
    #Read files to be processed
    replacetxt = ''
    replacetextlines = []
    
            
 # Try splitting into line by line so can intercaept title lines
    
    for replacetextline in originaltxt.split('\n'):
       
        #Do replacements, loop through each entry in list of replacements
        # Check line does not start with # - if it does, ignore
            
        if replacetextline== None:
            replacetextlines = replacetextlines.append('\n') 
        elif replacetextline.startswith('#'):
            replacetextlines.append(replacetextline) 
        else:
            for replacestring in replacementlist:
                newtext = replacetextline.replace(replacestring [0],replacestring [1])
                replacetextline = newtext
            replacetextlines.append(replacetextline)      

        
    #return new text
    replacetxt = replacetxt + '\n' + '\n'.join(replacetextlines)
    return replacetxt

