# Detailed revision log for schema changes to v0-5-0

Changes from schema v0.2.2

WIP - live version

## General

The following are general changes made that are not individually recorded here,
but where changes have been made these are indicated by red text in the spreadsheet:

- Minor typo changes that do not change meaning.

- Minor text improvements that clarify but do not change meaning.

- Status of some attributes changed to/from recommended

- Some examples amended, to make them more consistent and appropriate.

## Root

#### file

Changes to some attributes and descriptions to mirror changes made in agsProjectDocument, given below.
Includes new *systemURI* attribute.

Issue #35 considered. Attributes deleted:
- *intendedUsage*
- *limitations*
- *softwareUsed*

Attribute *documentRef* renamed *reference* - for consistency


## Model

#### agsiModel

new attribute - *uncertainty* - for short statement on uncertainty. Decided it is worth having something explicit in addition to *usageDesc*.
Note the example provided.

*modelID* is no longer 'required', but recommended. The schema does not actually need it!

*name* renamed *modelName* and *type* renamed *modelType* - Issue #15

Attributes renamed (to simplify/consistency):
- *inputDesc* changed to *input*
- *modelMethodDesc* changed to *method*
- *usageDesc* changed to *usage*

*documentID* renamed *documentSetID* and changed to string type - to tie in with changes to documents and sets thereof (see Project section below)

#### agsiModelElement

*elementID* is no longer 'required', but recommended. The schema does not actually need it!

new attribute - *elementName* - this is intended to be use as the short name/short description of the element.
The existing *description* attribute is retained but is now to be used for a more verbose description appropriate to the element type.

*type* renamed *modelType* - Issue #15.

*featureID* deleted. It was ahead of its time - to return when agsiFeature is added.

Note: the proposed new *narrative* and *narrativeURI* are no longer included in v0-5-0

#### agsiModelBoundary

New attributes - *topElevation* and *topSurfaceGeometryID* - see Issue #36.

Attributes renamed to be more consistent with other parts of the schema:
- *baseElevation* changed to *bottomElevation*
- *boundingPolygon* changed to *boundaryXYGeometryID*
- *baseSurface* changed to *bottomSurfaceGeometryID*



## Geometry

#### General

Examples revised to remove "-" in codes , and swap London for Gotham City!

#### agsiGeometryLine

attribute deleted: *is3D* - seems superflous

#### agsiGeometryFromFile

new attribute: *date* (of file)

Attributes renamed for consistency:
- *fileVersion* changed to *revision*
- *fileVersionInfo* changed to *revisionInfo*

example for *revision* simplified to only the revision ref


#### agsiGeometryExpHole

new attribute *investigationID* - this is used for reference to *agsProjectInvestigation* object, if used.

Existing *investigation* attribute retained but meaning amended so it is now only a text alternative to *investigationID*
to be used when agsProjectInvestigation is not in use.
Previously it was allowed to be either a reference or just text, which is not ideal.
Recommendation is for one or other to be used, but not both.

Clarified that geometryType and fileFormat should use term from vocab.
Suitable terms provided in the code and vocab section

Attributes renamed:
- *expHoleID* changed to *holeID* - for consistency (with below)
- *name* changed to *holeName* - (Issue #15)
- *type* changed to *holeType* and marked as recommended (Issue #15)

*profileCoordinates* - precedence rule added to attribute description


#### agsiGeometryColumn

*bottomCoordinate* - precedence rule added to attribute description


## Project



#### agsProjectCodeSet & agsProjectCode

Both new objects. To be used to cover things like project specific geology codes (typically inherited from AGS factual)
agsProjectCodeSet used to organised the codes into sets, each of which is used only for a specified object/attribute.
agsProjectCodeSet allows the option to identify an external source for the code list as an alternative to including individual codes.


#### agsProjectDocumentSet

This is a new object. See Issue #62 for discussion. Simple object, a collection of agsProjectDocument objects (which we already had - they have now gained a parent.
It is now this object that will be referenced from other parts of the schema - those refs have been checkedd and changed as required. Solution for document now adopted is very similar to that for AGS file sets.

#### agsProjectDocument

This is now a child object of agsProjectDocumentSet.

attribute deleted: *documentID* - no longer required as now embedded in new agsProjectDocumentSet object

attributes renamed:
- *originalAuthor* to *author*
- *originalClient* to *client*

attribute deleted: *suitabilityType* - see Issue #53 (JC OneNote comments)

*stutabilityCode* renamed *statusCode* and description amended - see Issue #53 (JC OneNote comments)

new attribute: *documentSystemURI* - we now allow two links to the document to be provided: local (e.g. if doc provided with file) and link to document management system location.

#### agsProject

*name* renamed *projectName* (Issue #15)

new attribute - *briefDocumentSetID* - link to brief/spec as discussed

new attribute - *reportDocumentSetID* - link to project output report - previously missing

attribute deleted: *agsProjectCoordinateSystems*.

attribute amended: *agsProjectCoordinateSystem* is now array, and can be used for one or more systems.

The previous system of using *agsProjectCoordinateSystem* for the default system was over-complicated. Now we will simply list the systems.

Attributes for new embedded objects as per above created/updated.

#### agsProjectCoordinateSystem

This has been fully revised, with many amended attributes and some new.  Main objectives of revision were:

- clarify terminology (we now talk about model, not project systems - other pages to be updated to suit)
- removed use of tuples for input (too fiddly, no big deal to add attributes)
- transform attributes reviewed and revised - these should now reflect common practice (based on LU standard 1-026)

Issues addressed include #15, #63, #64

#### agsProjectInvestigation

new attribute: *investgationID* - required for reference. Previously omitted in error.

attribute deleted: *isTheProject* - was making this overly complicated.

*name* renamed *investigationName* (Issue #15)

*reportDocumentID* renamed *reportDocumentSetID* now that we are referencing doucment sets - see above.

new attribute: *specificationDocumentSetID* - self explantory

new attribute: *dataDocumentSetID* - for (usually) the AGS factual data

Examples added where previously missing.

## Data

#### agsiData

attribute added - *documentSetID* - allows ref to GIR/GDR

attribute added - *remarks* - for consistency

#### agsiDataPropertyValue

Note: the proposed new *narrative* and *narrativeURI* are no longer included in v0-5-0

#### agsiDataParameterValue

Note: the proposed new *narrative* and *narrativeURI* are no longer included in v0-5-0

#### agsiDataCode

*isProjectSpecific* renamed *isStandard* - and meaning swapped (so that omission default = FALSE = non standard, ie safe assumption). Considered removing this but elected to keep it.
