


# AGSi Standard / Geometry

## agsiGeometryLine

### Object description

An [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, the plane that the line lies in will be determined by the context of referencing object. Lines comprise straight segments. Curves are not supported.

The parent object of [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)

[agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [coordinates](#coordinates)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by Model and Geometry objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GeometryAreaWest``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Area polygon for west zone``

#### coordinates
Co-ordinates of a 2D or 3D line, as an ordered list of co-ordinate tuples. What the co-ordinates represent is determined by the referencing object. To represent a polygon, the first and last [coordinate tuple](./Standard_General_Formats.md#coordinate-tuple)s must be identical.  
*Type:* array (of [coordinate tuple](./Standard_General_Formats.md#coordinate-tuple)s)  
*Condition:* Required  
*Example:* ``[ [1100,2250], [1200,2350], [1350, 2200], [1250, 2100], [1100,2250] ]``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
