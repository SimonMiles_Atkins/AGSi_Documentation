


# AGSi Standard / Data

## agsiDataParameterSet

### Object description

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) objects are containers for sets of parameter data, with each set typically corresponding to the parameters required for a model element. Sets may be referenced from [agsiModelElement](./Standard_Model_agsiModelElement.md) objects.

The parent object of [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) contains the following embedded child objects: 

- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) has the following attributes:


- [parameterSetID](#parametersetid)
- [description](#description)
- [remarks](#remarks)
- [agsiDataParameterValue](#agsidataparametervalue)


### Attributes

#### parameterSetID
Identifier that will be referenced by [agsiModelElement](./Standard_Model_agsiModelElement.md). All identifiers used by [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) must be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``ParamGCC/W``

#### description
Short description identifying the data set.  
*Type:* string  
*Example:* ``Gotham City Clay, west zone``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

#### agsiDataParameterValue
Array of embedded documentSetID object(s)  
*Type:* array (agsiDataParameterValue object(s))  


