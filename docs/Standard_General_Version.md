# AGSi Standard / General

## Version information

This is version **0.5.0-beta** of the Standard and schema, which is the first public release.

!!! Note
    The [Codes and vocabularies](./Codes_Intro.md) and
    [Guidance](./Guidance_Intro.md) have separate version control applied at page level.

This page will be used as a revision log.
