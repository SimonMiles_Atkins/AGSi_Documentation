# AGSi Standard / General


## Definitions

This section presents definitions of selected important terms used in this Standard or the associated Codes and vocabularies.

Some of the terms are defined elsewhere in the Standard but the are repeated here for ease of reference.

Some of the definitions are specific to AGSi, or are commonly understood terms. Where definitions have been adopted based on other external sources, these are identified.

!!! Note
    Some of the terms have been adopted from the terminology task work carried out by the *Integrated Digital Built Environment (IDBE) Geotechnical Engineering standardization* group, a collaboration between buildingSMART and the Open Geotepatial Consortium (OGC).

    The IDBE work is unpublished at the time of writing but work in progress can be viewed at the <a href="https://github.com/opengeospatial/IDBE-Geotech" target="_blank">IDBE Github repository</a>*

The terms are presented in alphabetical order.


#### Analytical model
Model suitable for direct use in analysis or design for a specified purpose. Normally based on development and interpretation of an observational model taking into account uncertainty, requirements of Codes and analysis/design methodology.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

#### Category (of model)
A classification system for the type of model based recommendations by
<a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>. The category may be: [conceptual](#conceptual-model),
[observational](#observational-model) or
[analytical](#analytical-model), as defined herein.
<br>*Source: IDBE*

#### Complementary data

Selected field observations or test results from ground investigations, often factual data but sometimes incoporating interpretation, included within a [model](#model) for information and/or visualisation.


#### Conceptual model
Abstract description of a system, such as geology or hydrogeology,
based on understanding of local and regional processes and relationships.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

#### (AGSi) document

Files that may include documents, data files, drawings, (BIM) models etc., identified and referenced as document objects ([agsProjectDocument](./Standard_Project_agsProjectDocument.md)) in the AGSi [file](#agsi-file).

#### Domain (of model)
A specified sphere of activity or knowledge. In the context of AGSi, the domains include ground (parent domain), geology, geotechnical, hydrogeology and geoenvironmental.
<br>*Source: IDBE*

#### Encoding

The process by which the data, arranged in accordance with the [schema](#agsi-schema), is converted to a specified format for transmittal. Encoding is also the term use for the deliverable produced.

#### Engineering geology observational model

Model showing  the anticipated location and extent of geological units and other geological features based on observations, eg. interpretation of borehole data.
<br>*Source: IDBE*

#### Factual data

The record of observations and measurements made during a ground investigation, which includes the results of field observations, in situ testing, laboratory testing and monitoring. Typically reported in a factual ground investigation report.

#### Interpreted data

Information, including but not limited to properties and/or parameters, collated and/or derived from factual data.
May incorporate some or all of presentation and evaluation of the factual information, technical insight, and parameters for analysis and design.

#### (AGSi) file

A data file produced in accordance with the [schema](#agsi-schema) and [encoding](#encoding) requirements defined in this Standard.

#### (AGSi) file/data set

The AGSi [file](#agsi-file) together with its [supporting files](#agsi-supporting-files) and [documents](#agsi-document).


#### Geological model

Abbreviated term for an [engineering geology observational
model](#engineering-geology-observational-model).
<br>*Source: IDBE*

#### Geotechnical design model

A geotechnical [analytical model](#analytical-model) intended for use in design. This model will define, usually for a specified purpose, the location and extent of geological/geotechnical units and their parameters.
<br>*Source: IDBE*

#### Geotechnical model

Abbreviated form of [geotechnical design model](#geotechnical-design-model).
<br>*Source: IDBE*

#### Global coordinate system

A secondary coordinate system, normally an established regional or national system, defined via a transformation from the [model coordinate system](#model-coordinate-system).

#### Ground model

See [model](#model). For AGSi, ground model is used as a generic parent term for any [type of model](#type-of-model) in the ground [domain](#domain-of-model).

#### Ground investigation

A campaign of fieldwork and/or laboratory testing carried out and reported under a site/ground investigation contract.

#### Hydrogeological model
Acceptable abbreviated description for an hydrogeological observational
(or analytical) model.
This model will show the anticipated location and extent of hydrogeological
units, e.g. aquifers.
<br>*Source: IDBE*

#### (AGSi) included files

[Supporting files](#agsi-supporting-files) and [documents](#agsi-document) that are transferred or shared with the AGSi [file](#agsi-file).

#### Investigation

See [ground investigation](#ground-investigation).

#### Model

A digital geometric (2D or 3D) representation of the ground.

#### Model boundary

Definition of valid extent of a model. Model geometry lying outside of a model boundary should be ignored.

#### Model coordinate system

The coordinate system used by a model, although this could be an established regional or national system.

#### Model element

Component part of a [model](#model) in AGSi,
ie. a [model](#model) is a collection of model elements.

#### Observational model

Model predominantly based on observations and measurements of the ground. This may incorporate interpretation of anticipated conditions in areas between the observation and measurement points. Generally not suitable for
direct use in analysis or design without further interpretation. Observational models should be developed from a conceptual model.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

#### Parameter

An interpreted single value, or a profile of values
related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.
<br>*Source: IDBE*

#### Project

The Project is the specific project/commission under which the AGSi file/data set has been delivered.

#### Parent project

The project that commissioned the [Project](#project) or, more generally, any project that commissions another project.

#### Producer

A person or organisation responsible for creating and delivering the AGSi [file/data set](#agsi-filedata-set). A producer is a [user](#user).

#### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.
<br>*Source: IDBE*

#### (AGSi) schema

The organisation/structure of AGSi data. Includes data hierarchy, object and attribute names and definitions, required data types and rules for use.

#### Specifier

A person or organisation that is responsibility for specifying the AGSi [file/data set](#agsi-filedata-set) to be delivered. A specifier is a [user](user).

#### (AGSi) supporting files

Files that provide data or information referenced by the AGSi [file](#agsi-file), e.g. geometry, that is required to build the full model(s).

#### Type (of model)

Concise definition incorporating the
[category](#category-of-model) and [domain](#domain-of-model) of the model,
and any further definition that may be required.
Use of the formally defined category and domain names is preferred,
but some acceptable alternative terms are included herein.
<br>*Source: IDBE*

#### Ultimate project

The top level parent project of the [Project](#project), typically the works that are eventually to be constructed that will benefit from the [Project](#project) or a framework contract set up by an asset owner.

#### URI

A Uniform Resource Identifier (URI) is a string of characters that unambiguously identify a particular resource. Used in computing to identify the location of files, websites etc. A URI is formatted in accordance with
<a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>. A URL (Uniform Resource Locator) is a subset of URI.

#### User

A person or organisation that is or is expected to specify, create or make use of an [AGSi file/data set](#agsi-filedata-set).

#### UUID

A universally unique identifier (UUID) is a 128-bit number used to identify information in computer systems. The term globally unique identifier (GUID) is also often used.
