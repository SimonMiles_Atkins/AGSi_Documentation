# AGSi Codes and vocabularies

## Code list

This page provides lists of recommended codes for use by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)

These codes are not intended for use with
[agsProjectCode](./Standard_Project_agsProjectCode.md) and
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).
Refer to
[Project general rules](./Standard_Project_Rules.md)
for recommended codes for use with
[agsProjectCode](./Standard_Project_agsProjectCode.md)

!!! Note
    The following are first draft lists provided for the beta release of AGSi. They are by no means complete, and they may well change.
    AGS welcomes feedback including suggestions for new codes for items not yet covered.


### Parameters

Recommended codes for use by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md).

#### Geotechnical

The following are proposed geotechnical parameter codes,
ordered by category.
This is by no means a complete set of parameters, but it establishes a proposed style for such codes.


Code | Description | Units | Category
---- | ----------- | ----- | --------
`Depth` | Depth | m | General
`Elevation` | Elevation | m | General
`UnitWeightBulk` | Bulk unit weight | kN/m3 | Density
`AngleFriction` | Effective angle of shearing resistance | deg | Strength
`AngleFrictionPeak` | Peak effective angle of shearing resistance | deg | Strength
`AngleFrictionCritical` | Critical state effective angle of shearing resistance | deg | Strength
`AngleFrictionResidual` | Residual effective angle of shearing resistance | deg | Strength
`Cohesion` | Effective cohesion | kPa | Strength
`UndrainedShearStrength` | Undrained shear strength | kPa | Strength
`UndrainedShearStrengthTriaxial` | Undrained shear strength from triaxial tests | kPa | Strength
`UniaxialCompressiveStrength` | Uniaxial Compressive Strength | MPa | Stiffness
`YoungsModulusDrained` | Drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrained` | Undrained Young's Modulus | MPa | Stiffness
`YoungsModulusDrainedVertical` | Vertical drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrainedVertical` | Vertical undrained Young's Modulus | MPa | Stiffness
`YoungsModulusDrainedHorizontal` | Horizontal drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrainedHorizontal` | Horizontal undrained Young's Modulus | MPa | Stiffness
`BulkModulus` | Bulk modulus | MPa | Stiffness
`ShearModulusDrained` | Drained shear Modulus | MPa | Stiffness
`ShearModulusUndrained` | Undrained shear Modulus | MPa | Stiffness
`CBR` | California bearing ratio (CBR) | % | Pavement
`SubgradeSurfaceModulus` | subgrade surface modulus | MPa | Pavement
`ACECClass` | ACEC Aggressive chemical environment class |  | ACEC
`ACECDSClass` | ACEC Design sulphate class |  | ACEC
`ACECCDCClass` | ACEC Design chemical class |  | ACEC

#### Other domains

!!! Note
    Codes for other domains to be developed.

## Properties

Recommended codes for use by
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md).

#### Geotechnical / Geoenvironmental

Properties generally relate to factual GI data. Therefore the following is recommended.

Subject to the exceptions and additions described below, the property code shall be the **heading name** for the applicable property defined in the <a href="https://www.ags.org.uk/" target="_blank">AGS (factual) data format</a>.

Examples:

* `LLPL_PL` for plastic limit
* `ISPT_NVAL` for SPT N value

For geochemical data from the AGS (factual) GCHM group, use the ABBR code used for the determinand (GCHM_CODE).

For geoenvironmental data from the AGS (factual) ERES group, use the ABBR code used for the chemical code (GCHM_CODE).

Where the AGS (factual) data format heading is insufficient, e.g. reporting of derived values, or summary of values from different types of test, use the codes recommended for parameters.

For depth and elevation where required, e.g. profiles, the following are recommended:

* `Depth` for depth
* `Elevation` for elevation

If additional codes are required, it is recommended that the style adopted for parameter codes is adopted.
